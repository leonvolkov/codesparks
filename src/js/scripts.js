'use strict';
// piece object
let piece;
// If we are using arrow functions and other new ECMA features,
// let's use also classes
class Piece {
    constructor (el) {
        this.el = el;
        this.watchColor();
    }
    moveDelta(dx, dy) {
        const pos = this.el.getBoundingClientRect();
        let left = pos.left + dx;
        let top = pos.top + dy;
        if (left < 0) {
            left = 0;
        } else if (left > window.innerWidth - this.el.offsetWidth) {
            left = window.innerWidth - this.el.offsetWidth;
        }
        if (top < 0) {
            top = 0;
        } else if (top > window.innerHeight - this.el.offsetHeight) {
            top = window.innerHeight - this.el.offsetHeight;
        }
        this.el.style.left = `${left}px`;
        this.el.style.top = `${top}px`;
    }
    moveRandom() {
        const left = Math.floor(Math.random() * (window.innerWidth - this.el.offsetWidth + 1));
        const top = Math.floor(Math.random() * (window.innerHeight - this.el.offsetHeight + 1));
        this.el.style.left = `${left}px`;
        this.el.style.top = `${top}px`;
    };
    reset() {
        this.el.style.left = `50%`;
        this.el.style.top = `100px`;
    }
    watchColor() {
        // check and change color by temperature every 10 minutes
        this.setColor();
        setInterval(this.setColor.bind(this), 1000*60*10);
    }
    // Doesn't work in IE, but very nice syntax
    // ES8 async/await feature
    async setColor() {
        let temp;
        try {
            // get weather info
            const response = await fetch('http://api.apixu.com/v1/current.json?key=dda6e762ae4f41efb7e173552192204&q=tel%20aviv');
            // parse JSON
            const data = await response.json();
            if (data.current && data.current.temp_c) {
                temp = data.current.temp_c;
            }
        } catch (err) {
            console.log(err);
        }
        const color = this.getColorByTemperature(temp);
        this.el.style.backgroundColor = this.el.style.borderColor  = color;
    }
    getColorByTemperature(sTemp) {
        let color;
        const temp = parseInt(sTemp);
        if (temp <= 10) {
            color = 'blue';
        } else if (temp <= 20) {
            color = 'green';
        } else if (temp <= 30) {
            // instead of annoying 'yellow'
            color = '#FFDE30';
        } else if (temp > 30) {
            color = 'red';
        } else {
            // in case of NaN
            color = 'gray';
        }
        return color;
    }
}

function handleClick(ev) {
    piece.moveDelta(parseInt(this.dataset.dx), parseInt(this.dataset.dy));
}

function setupClick(elId, dataset) {
    const $btn = document.getElementById(elId);
    let handler;
    if (typeof dataset === 'object') {
        $btn.dataset.dx = dataset.dx;
        $btn.dataset.dy = dataset.dy;
        handler = handleClick;
    } else if (typeof dataset === 'function') {
        handler =  dataset;
    }
    $btn.addEventListener("click", handler);
}

function init() {
    const buttonsData = {
        "btn-up": {dx: 0, dy: -100},
        "btn-right": {dx: 100, dy: 0},
        "btn-down": {dx: 0, dy: 100},
        "btn-left": {dx: -100, dy: 0},
        "btn-reset": piece.reset.bind(piece),
        "btn-random": piece.moveRandom.bind(piece)
    };
    for (const btnData in buttonsData) {
        if (buttonsData.hasOwnProperty(btnData)) {
            setupClick(btnData, buttonsData[btnData]);
        }
    }
}

window.addEventListener("DOMContentLoaded", event => {
    piece = new Piece(document.getElementById("piece"));
    window.addEventListener('resize', piece.reset.bind(piece));
    init();
});
