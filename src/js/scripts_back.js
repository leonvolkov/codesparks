// piece object
const piece = (function() {
    let el = null;
    const init = function(el) {
        this.el = el;
    };
    const moveDelta = function(dx, dy) {
        const pos = this.el.getBoundingClientRect();
        this.el.style.left = `${pos.left + dx}px`;
        this.el.style.top = `${pos.top + dy}px`;
    };
    return {
        init,
        moveDelta
    };
})();

function handleClick(ev) {
    piece.moveDelta(parseInt(this.dataset.dx), parseInt(this.dataset.dy));
}

function setupClick(elId, dx, dy) {
    const $btn = document.getElementById(elId);
    $btn.dataset.dx = dx;
    $btn.dataset.dy = dy;
    $btn.addEventListener("click", handleClick);
}

function init() {
    setupClick("btn-up", 0, -100);
    setupClick("btn-right", 100, 0);
    setupClick("btn-down", 0, 100);
    setupClick("btn-left", -100, 0);

    setupClick("btn-reset", -100, 0);
    setupClick("btn-random", -100, 0);
}

window.addEventListener("DOMContentLoaded", event => {
    piece.init(document.getElementById("piece"));
    init();
});

const getTemperature = async () => {
    let temp;
    try {
        // get weather info
        const response = await fetch('http://api.apixu.com/v1/current.json?key=dda6e762ae4f41efb7e173552192204&q=tel%20aviv');
        // parse JSON
        const data = await response.json();
        if (data.current && data.current.temp_c) {
            temp = data.current.temp_c;
        }
    } catch (err) {
        console.log(err);
    }
    const color = getColorByTemperature(temp);
    piece.el.style.backgroundColor = piece.el.style.borderColor  = color;
};

function getColorByTemperature(sTemp) {
    // in case of NaN
    let sColor = 'gray';
    const temp = parseInt(sTemp);
    if (temp <= 10) {
        sColor = 'blue';
    } else if (temp <= 20) {
        sColor = 'green';
    } else if (temp <= 30) {
        sColor = 'green';
    } else if (temp > 30) {
        sColor = 'green';
    }
    return sColor;
}

getTemperature();